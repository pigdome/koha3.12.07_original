# LibLime Koha Translation Manager
# Copyright (C) 2007 LibLime
# http://liblime.com <info@liblime.com> 
# Based on Kartouche http://www.dotmon.com/kartouche/ 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2013-07-17 10:25-0300\n"
"PO-Revision-Date: 2008-03-22 21:11+0200\n"
"Last-Translator: translate.koha.org\n"
"Language-Team: Koha Translation Team <koha-translate@nongnu.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8-bit\n"
"X-Generator: Kartouche 0.1 - 22 October 2003\n"
"\n"

#. For the first occurrence,
#. %1$s:  END 
#: opac-tmpl/ccsr/en/includes/masthead.inc:85
#: opac-tmpl/ccsr/en/includes/masthead.inc:108
#: opac-tmpl/ccsr/en/includes/masthead.inc:109
#: opac-tmpl/ccsr/en/includes/masthead.inc:116
#: opac-tmpl/ccsr/en/includes/masthead.inc:118
#: opac-tmpl/ccsr/en/includes/masthead.inc:194
#: opac-tmpl/ccsr/en/includes/masthead.inc:202
#: opac-tmpl/ccsr/en/includes/masthead.inc:226
#: opac-tmpl/ccsr/en/includes/masthead.inc:227
#: opac-tmpl/ccsr/en/includes/masthead.inc:247
#: opac-tmpl/ccsr/en/includes/masthead.inc:248
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:97
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:30
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:31
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:58
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:102
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:103
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:104
#: opac-tmpl/ccsr/en/includes/top-bar.inc:21
#: opac-tmpl/ccsr/en/includes/top-bar.inc:34
#: opac-tmpl/ccsr/en/includes/top-bar.inc:66
#: opac-tmpl/ccsr/en/includes/usermenu.inc:3
#: opac-tmpl/ccsr/en/includes/usermenu.inc:4
#: opac-tmpl/ccsr/en/includes/usermenu.inc:6
#: opac-tmpl/ccsr/en/includes/usermenu.inc:8
#: opac-tmpl/ccsr/en/includes/usermenu.inc:10
#: opac-tmpl/ccsr/en/includes/usermenu.inc:13
#: opac-tmpl/ccsr/en/includes/usermenu.inc:16
#: opac-tmpl/ccsr/en/includes/usermenu.inc:19
#: opac-tmpl/ccsr/en/includes/usermenu.inc:21
#: opac-tmpl/ccsr/en/includes/usermenu.inc:26
#: opac-tmpl/ccsr/en/includes/usermenu.inc:30
#: opac-tmpl/ccsr/en/includes/usermenu.inc:33
#, fuzzy, c-format
msgid "%s"
msgstr "%1$s;"

#. For the first occurrence,
#. %1$s:  ELSE 
#: opac-tmpl/ccsr/en/includes/masthead.inc:14
#: opac-tmpl/ccsr/en/includes/masthead.inc:20
#: opac-tmpl/ccsr/en/includes/masthead.inc:27
#: opac-tmpl/ccsr/en/includes/masthead.inc:34
#: opac-tmpl/ccsr/en/includes/masthead.inc:39
#: opac-tmpl/ccsr/en/includes/masthead.inc:45
#: opac-tmpl/ccsr/en/includes/masthead.inc:47
#: opac-tmpl/ccsr/en/includes/masthead.inc:49
#: opac-tmpl/ccsr/en/includes/masthead.inc:51
#: opac-tmpl/ccsr/en/includes/masthead.inc:53
#: opac-tmpl/ccsr/en/includes/masthead.inc:58
#: opac-tmpl/ccsr/en/includes/masthead.inc:63
#: opac-tmpl/ccsr/en/includes/masthead.inc:68
#: opac-tmpl/ccsr/en/includes/masthead.inc:73
#: opac-tmpl/ccsr/en/includes/masthead.inc:78
#: opac-tmpl/ccsr/en/includes/masthead.inc:83
#: opac-tmpl/ccsr/en/includes/masthead.inc:89
#: opac-tmpl/ccsr/en/includes/masthead.inc:91
#: opac-tmpl/ccsr/en/includes/masthead.inc:93
#: opac-tmpl/ccsr/en/includes/masthead.inc:99
#: opac-tmpl/ccsr/en/includes/masthead.inc:101
#: opac-tmpl/ccsr/en/includes/masthead.inc:111
#: opac-tmpl/ccsr/en/includes/masthead.inc:116
#: opac-tmpl/ccsr/en/includes/masthead.inc:121
#: opac-tmpl/ccsr/en/includes/masthead.inc:147
#: opac-tmpl/ccsr/en/includes/masthead.inc:152
#: opac-tmpl/ccsr/en/includes/masthead.inc:158
#: opac-tmpl/ccsr/en/includes/masthead.inc:160
#: opac-tmpl/ccsr/en/includes/masthead.inc:162
#: opac-tmpl/ccsr/en/includes/masthead.inc:175
#: opac-tmpl/ccsr/en/includes/masthead.inc:177
#: opac-tmpl/ccsr/en/includes/masthead.inc:182
#: opac-tmpl/ccsr/en/includes/masthead.inc:190
#: opac-tmpl/ccsr/en/includes/masthead.inc:231
#: opac-tmpl/ccsr/en/includes/masthead.inc:240
#: opac-tmpl/ccsr/en/includes/masthead.inc:242
#: opac-tmpl/ccsr/en/includes/masthead.inc:244
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:1
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:2
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:4
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:25
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:28
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:30
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:32
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:35
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:55
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:67
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:131
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:10
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:16
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:22
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:26
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:43
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:140
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:141
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:144
#: opac-tmpl/ccsr/en/includes/top-bar.inc:17
#: opac-tmpl/ccsr/en/includes/top-bar.inc:19
#: opac-tmpl/ccsr/en/includes/top-bar.inc:21
#: opac-tmpl/ccsr/en/includes/top-bar.inc:32
#: opac-tmpl/ccsr/en/includes/top-bar.inc:34
#: opac-tmpl/ccsr/en/includes/top-bar.inc:37
#: opac-tmpl/ccsr/en/includes/top-bar.inc:54
#: opac-tmpl/ccsr/en/includes/top-bar.inc:56
#: opac-tmpl/ccsr/en/includes/top-bar.inc:66
#: opac-tmpl/ccsr/en/includes/top-bar.inc:67
#: opac-tmpl/ccsr/en/includes/usermenu.inc:33
#, fuzzy, c-format
msgid "%s "
msgstr "%1$s;"

#. For the first occurrence,
#. %1$s:  FOREACH BranchesLoo IN BranchesLoop 
#. %2$s:  IF ( BranchesLoo.selected ) 
#: opac-tmpl/ccsr/en/includes/masthead.inc:106
#: opac-tmpl/ccsr/en/includes/masthead.inc:195
#: opac-tmpl/ccsr/en/includes/masthead.inc:196
#: opac-tmpl/ccsr/en/includes/masthead.inc:197
#: opac-tmpl/ccsr/en/includes/masthead.inc:198
#: opac-tmpl/ccsr/en/includes/masthead.inc:199
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:28
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:30
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:56
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:129
#: opac-tmpl/ccsr/en/includes/top-bar.inc:64
#: opac-tmpl/ccsr/en/includes/usermenu.inc:4
#: opac-tmpl/ccsr/en/includes/usermenu.inc:6
#: opac-tmpl/ccsr/en/includes/usermenu.inc:8
#: opac-tmpl/ccsr/en/includes/usermenu.inc:19
#, fuzzy, c-format
msgid "%s %s"
msgstr "%s%s 환영합니다"

#. For the first occurrence,
#. %1$s:  UNLESS ( opacsmallimage ) 
#. %2$s:  UNLESS ( LibraryName ) 
#: opac-tmpl/ccsr/en/includes/masthead.inc:11
#: opac-tmpl/ccsr/en/includes/masthead.inc:22
#: opac-tmpl/ccsr/en/includes/masthead.inc:36
#: opac-tmpl/ccsr/en/includes/masthead.inc:55
#: opac-tmpl/ccsr/en/includes/masthead.inc:60
#: opac-tmpl/ccsr/en/includes/masthead.inc:65
#: opac-tmpl/ccsr/en/includes/masthead.inc:70
#: opac-tmpl/ccsr/en/includes/masthead.inc:75
#: opac-tmpl/ccsr/en/includes/masthead.inc:80
#: opac-tmpl/ccsr/en/includes/masthead.inc:95
#: opac-tmpl/ccsr/en/includes/masthead.inc:109
#: opac-tmpl/ccsr/en/includes/masthead.inc:113
#: opac-tmpl/ccsr/en/includes/masthead.inc:118
#: opac-tmpl/ccsr/en/includes/masthead.inc:149
#: opac-tmpl/ccsr/en/includes/masthead.inc:171
#: opac-tmpl/ccsr/en/includes/masthead.inc:203
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:7
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:31
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:49
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:112
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:120
#: opac-tmpl/ccsr/en/includes/top-bar.inc:29
#: opac-tmpl/ccsr/en/includes/top-bar.inc:41
#: opac-tmpl/ccsr/en/includes/top-bar.inc:44
#: opac-tmpl/ccsr/en/includes/top-bar.inc:58
#: opac-tmpl/ccsr/en/includes/top-bar.inc:60
#, fuzzy, c-format
msgid "%s %s "
msgstr "%s%s 환영합니다"

#. For the first occurrence,
#. %1$s:  END 
#. %2$s:  IF ( suggestion ) 
#. %3$s:  IF ( AnonSuggestions ) 
#: opac-tmpl/ccsr/en/includes/masthead.inc:200
#: opac-tmpl/ccsr/en/includes/usermenu.inc:10
#: opac-tmpl/ccsr/en/includes/usermenu.inc:13
#: opac-tmpl/ccsr/en/includes/usermenu.inc:16
#: opac-tmpl/ccsr/en/includes/usermenu.inc:30
#, fuzzy, c-format
msgid "%s %s %s"
msgstr "%s%s 환영합니다"

#. For the first occurrence,
#. %1$s:  IF ( opacheader ) 
#. %2$s:  opacheader 
#. %3$s:  END 
#: opac-tmpl/ccsr/en/includes/masthead.inc:3
#: opac-tmpl/ccsr/en/includes/masthead.inc:16
#: opac-tmpl/ccsr/en/includes/masthead.inc:30
#: opac-tmpl/ccsr/en/includes/masthead.inc:125
#: opac-tmpl/ccsr/en/includes/masthead.inc:129
#: opac-tmpl/ccsr/en/includes/masthead.inc:133
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:38
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:45
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:51
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:57
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:62
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:77
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:12
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:18
#, fuzzy, c-format
msgid "%s %s %s "
msgstr "%s%s 환영합니다"

#. %1$s:  END 
#. %2$s:  END 
#. %3$s:  IF ( EnhancedMessagingPreferences ) 
#. %4$s:  IF ( messagingview ) 
#: opac-tmpl/ccsr/en/includes/usermenu.inc:26
#, fuzzy, c-format
msgid "%s %s %s %s"
msgstr "%s%s 환영합니다"

#. %1$s:  END 
#. %2$s:  END 
#. %3$s:  IF ( suggestion ) 
#. %4$s:  UNLESS ( AnonSuggestions ) 
#. %5$s:  IF ( suggestionsview ) 
#: opac-tmpl/ccsr/en/includes/usermenu.inc:21
#, fuzzy, c-format
msgid "%s %s %s %s %s"
msgstr "%s%s 환영합니다"

#. For the first occurrence,
#. %1$s:  IF IsPatronPage 
#. %2$s:  INCLUDE usermenu.inc 
#. %3$s:  END 
#. %4$s:  OpacNav 
#. %5$s:  OpacNavBottom 
#: opac-tmpl/ccsr/en/includes/navigation.inc:1
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:69
#, fuzzy, c-format
msgid "%s %s %s %s %s "
msgstr "%s%s 환영합니다"

#. For the first occurrence,
#. %1$s:  sublanguages_loo.native_description 
#. %2$s:  sublanguages_loo.script_description 
#. %3$s:  sublanguages_loo.region_description 
#. %4$s:  sublanguages_loo.variant_description 
#. %5$s:  sublanguages_loo.rfc4646_subtag 
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:55
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:57
#, c-format
msgid "%s %s %s %s (%s)"
msgstr ""

#. %1$s:  IF ( related ) 
#. %2$s:  FOREACH relate IN related 
#. %3$s:  relate.related_search 
#. %4$s:  END 
#. %5$s:  END 
#: opac-tmpl/ccsr/en/includes/masthead.inc:228
#, fuzzy, c-format
msgid "%s (related searches: %s%s%s). %s "
msgstr "상세검색"

#. %1$s:  ELSE 
#. %2$s:  END 
#. %3$s:  IF ( opacuserlogin ) 
#: opac-tmpl/ccsr/en/includes/top-bar.inc:24
#, fuzzy, c-format
msgid "%s No public lists %s %s "
msgstr "출판되지 않았습니다"

#. %1$s:  IF ( searchdesc ) 
#. %2$s:  LibraryName 
#: opac-tmpl/ccsr/en/includes/masthead.inc:234
#, fuzzy, c-format
msgid "%s No results found for that in %s catalog. "
msgstr "목록내 찾을 수 없습니다."

#. %1$s:  ELSE 
#. %2$s:  END 
#: opac-tmpl/ccsr/en/includes/masthead.inc:236
#, fuzzy, c-format
msgid "%s You did not specify any search criteria. %s "
msgstr "귀하는 아무런 검색기준을 지정하지 않았습니다."

#. For the first occurrence,
#. %1$s:  ELSE 
#. %2$s:  IF ( virtualshelves ) 
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:104
#, fuzzy, c-format
msgid "%s%s"
msgstr "%s%s 환영합니다"

#. %1$s:  IF ( opacuserlogin ) 
#. %2$s:  IF ( loggedinusername ) 
#: opac-tmpl/ccsr/en/includes/usermenu.inc:1
#, fuzzy, c-format
msgid "%s%s "
msgstr "%s%s 환영합니다"

#. %1$s:  FOREACH USER_INF IN USER_INFO 
#. %2$s:  USER_INF.title 
#. %3$s:  USER_INF.firstname 
#. %4$s:  USER_INF.surname 
#. %5$s:  END 
#: opac-tmpl/ccsr/en/includes/top-bar.inc:60
#, fuzzy, c-format
msgid "%s%s %s %s%s"
msgstr "%s%s 환영합니다"

#. %1$s:  IF ( opacuserjs ) 
#. %2$s:  opacuserjs 
#. %3$s:  END 
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:85
#, fuzzy, c-format
msgid "%s%s%s"
msgstr "%s%s 환영합니다"

#. %1$s:  END 
#. %2$s:  ELSE 
#. %3$s:  END 
#: opac-tmpl/ccsr/en/includes/usermenu.inc:37
#, fuzzy, c-format
msgid "%s%s%s "
msgstr "%s%s 환영합니다"

#. %1$s:  END 
#. %2$s:  ELSE 
#. %3$s:  END 
#. %4$s:  IF ( GoogleJackets ) 
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:105
#, fuzzy, c-format
msgid "%s%s%s %s "
msgstr "%s%s 환영합니다"

#. For the first occurrence,
#. %1$s:  IF ( languages_loo.native_description ) 
#. %2$s:  languages_loo.native_description 
#. %3$s:  ELSE 
#. %4$s:  languages_loo.rfc4646_subtag 
#. %5$s:  END 
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:50
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:67
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:69
#, fuzzy, c-format
msgid "%s%s%s%s%s"
msgstr "%s%s 환영합니다"

#. %1$s:  END 
#: opac-tmpl/ccsr/en/includes/top-bar.inc:66
#, fuzzy, c-format
msgid "%sLog Out"
msgstr "로그아웃"

#: opac-tmpl/ccsr/en/includes/masthead.inc:227
#, fuzzy, c-format
msgid "&rsaquo; "
msgstr "%1$s 목록--ISBD (국제표준서지기술)"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
#, fuzzy
msgid "Add to your cart"
msgstr "내 가상서가에 첨가하세요"

#: opac-tmpl/ccsr/en/includes/masthead.inc:194
#, fuzzy, c-format
msgid "Advanced search"
msgstr "상세검색"

#: opac-tmpl/ccsr/en/includes/masthead.inc:180
#, fuzzy, c-format
msgid "All Libraries"
msgstr "모든 분관"

#: opac-tmpl/ccsr/en/includes/masthead.inc:105
#, fuzzy, c-format
msgid "All libraries"
msgstr "모든 분관"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Are you sure you want to empty your cart?"
msgstr ""

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Are you sure you want to remove the selected items?"
msgstr ""

#: opac-tmpl/ccsr/en/includes/masthead.inc:63
#: opac-tmpl/ccsr/en/includes/masthead.inc:65
#, c-format
msgid "Author"
msgstr "저자"

#: opac-tmpl/ccsr/en/includes/masthead.inc:196
#, c-format
msgid "Authority search"
msgstr "전거(검정)검색"

#: opac-tmpl/ccsr/en/includes/masthead.inc:195
#, c-format
msgid "Browse by hierarchy"
msgstr ""

#: opac-tmpl/ccsr/en/includes/masthead.inc:83
#: opac-tmpl/ccsr/en/includes/masthead.inc:85
#, fuzzy, c-format
msgid "Call number"
msgstr "호출 번호"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
#, fuzzy
msgid "Cart"
msgstr "날짜"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:97
msgid ""
"Could not login, perhaps your Persona email does not match your Koha one"
msgstr ""

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Error! Illegal parameter"
msgstr ""

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Error! The add_tag operation failed on"
msgstr ""

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Error! You cannot delete the tag"
msgstr ""

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid ""
"Error! Your tag was entirely markup code. It was NOT added. Please try again "
"with plain text."
msgstr ""

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
#, fuzzy
msgid "Errors: "
msgstr "를 위해："

#. INPUT type=submit
#: opac-tmpl/ccsr/en/includes/masthead.inc:139
#: opac-tmpl/ccsr/en/includes/masthead.inc:186
msgid "Go"
msgstr ""

#. OPTGROUP
#: opac-tmpl/ccsr/en/includes/masthead.inc:113
msgid "Groups"
msgstr ""

#. A
#: opac-tmpl/ccsr/en/includes/masthead.inc:226
#, c-format
msgid "Home"
msgstr ""

#: opac-tmpl/ccsr/en/includes/masthead.inc:73
#: opac-tmpl/ccsr/en/includes/masthead.inc:75
#, c-format
msgid "ISBN"
msgstr ""

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
#, fuzzy
msgid "In your cart"
msgstr "책바구니로 보내기"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
#, fuzzy
msgid "Items in your cart: "
msgstr "책바구니로 보내기"

#. IMG
#: opac-tmpl/ccsr/en/includes/masthead.inc:14
#: opac-tmpl/ccsr/en/includes/masthead.inc:20
#, fuzzy
msgid "Koha Online Catalog"
msgstr "%1$s 목록--정기(연속)간행물"

#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:3
msgid "Koha [% Version %]"
msgstr ""

#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:44
#, fuzzy, c-format
msgid "Languages:&nbsp;"
msgstr "언어변경:"

#. OPTGROUP
#: opac-tmpl/ccsr/en/includes/masthead.inc:106
#, fuzzy
msgid "Libraries"
msgstr "모든 분관"

#: opac-tmpl/ccsr/en/includes/masthead.inc:165
#, fuzzy, c-format
msgid "Library Catalog"
msgstr "%1$s 장서목록"

#: opac-tmpl/ccsr/en/includes/masthead.inc:53
#: opac-tmpl/ccsr/en/includes/masthead.inc:55
#, fuzzy, c-format
msgid "Library catalog"
msgstr "%1$s 장서목록"

#: opac-tmpl/ccsr/en/includes/top-bar.inc:14
#, c-format
msgid "Lists"
msgstr ""

#: opac-tmpl/ccsr/en/includes/top-bar.inc:44
#, fuzzy, c-format
msgid "Log in to create your own lists"
msgstr "<a1>로그인</a>Koha"

#: opac-tmpl/ccsr/en/includes/top-bar.inc:58
#, fuzzy, c-format
msgid "Log in to your account"
msgstr "귀하의 계정으로 로인인 하세요"

#: opac-tmpl/ccsr/en/includes/masthead.inc:200
#, c-format
msgid "Most popular"
msgstr ""

#. For the first occurrence,
#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:45
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:52
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:108
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:116
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:125
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:133
#, fuzzy
msgid "No cover image available"
msgstr "복사할 수 없습니다."

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
#, fuzzy
msgid "No item was added to your cart"
msgstr "책바구니로 보내기"

#. For the first occurrence,
#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "No item was selected"
msgstr ""

#: opac-tmpl/ccsr/en/includes/top-bar.inc:40
#, fuzzy, c-format
msgid "No private lists"
msgstr "출판되지 않았습니다"

#: opac-tmpl/ccsr/en/includes/masthead.inc:233
#, fuzzy, c-format
msgid "No results found!"
msgstr "찾을 수 없습니다."

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "No tag was specified."
msgstr ""

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Note: you can only delete your own tags."
msgstr ""

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid ""
"Note: you can only tag an item with a given term once. Check 'My Tags' to "
"see your current tags."
msgstr ""

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid ""
"Note: your tag contained markup code that was removed. The tag was added as "
msgstr ""

#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:30
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:32
#, fuzzy, c-format
msgid "Powered by"
msgstr "%s에 의해 출판되었습니다"

#: opac-tmpl/ccsr/en/includes/top-bar.inc:17
#, fuzzy, c-format
msgid "Public lists"
msgstr "공중 책꽂이"

#: opac-tmpl/ccsr/en/includes/masthead.inc:202
#: opac-tmpl/ccsr/en/includes/masthead.inc:203
#, fuzzy, c-format
msgid "Purchase suggestions"
msgstr "구매제안"

#: opac-tmpl/ccsr/en/includes/masthead.inc:197
#, fuzzy, c-format
msgid "Recent comments"
msgstr "신착도서"

#. A
#: opac-tmpl/ccsr/en/includes/masthead.inc:227
#, c-format
msgid "Search"
msgstr "검색"

#. For the first occurrence,
#. %1$s:  UNLESS ( OpacAddMastheadLibraryPulldown ) 
#. %2$s:  IF ( mylibraryfirst ) 
#. %3$s:  mylibraryfirst 
#. %4$s:  END 
#. %5$s:  END 
#: opac-tmpl/ccsr/en/includes/masthead.inc:41
#: opac-tmpl/ccsr/en/includes/masthead.inc:154
#, c-format
msgid "Search %s %s (in %s only)%s %s "
msgstr ""

#: opac-tmpl/ccsr/en/includes/top-bar.inc:64
#, fuzzy, c-format
msgid "Search history"
msgstr "검색중"

#: opac-tmpl/ccsr/en/includes/masthead.inc:78
#: opac-tmpl/ccsr/en/includes/masthead.inc:80
#, fuzzy, c-format
msgid "Series"
msgstr "총서명"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
#, fuzzy
msgid "Sorry, tags are not enabled on this system."
msgstr "미안합니다. 이 표제에 대해서는 아무런 서평을 할 수 없습니다."

#: opac-tmpl/ccsr/en/includes/masthead.inc:68
#: opac-tmpl/ccsr/en/includes/masthead.inc:70
#, c-format
msgid "Subject"
msgstr "주제"

#: opac-tmpl/ccsr/en/includes/masthead.inc:199
#, fuzzy, c-format
msgid "Subject cloud"
msgstr "주제"

#. IMG
#: opac-tmpl/ccsr/en/includes/masthead.inc:231
#: opac-tmpl/ccsr/en/includes/masthead.inc:236
msgid "Subscribe to this search"
msgstr ""

#: opac-tmpl/ccsr/en/includes/masthead.inc:198
#, fuzzy, c-format
msgid "Tag cloud"
msgstr "너의 것。"

#. For the first occurrence,
#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Tags added: "
msgstr ""

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
#, fuzzy
msgid "This item has been added to your cart"
msgstr "책바구니로 보내기"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
#, fuzzy
msgid "This item has been removed from your cart"
msgstr "책바구니로 보내기"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
#, fuzzy
msgid "This item is already in your cart"
msgstr "책바구니로 보내기"

#: opac-tmpl/ccsr/en/includes/masthead.inc:58
#: opac-tmpl/ccsr/en/includes/masthead.inc:60
#, c-format
msgid "Title"
msgstr "표제"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
msgid "Unable to add one or more tags."
msgstr ""

#. A
#: opac-tmpl/ccsr/en/includes/top-bar.inc:64
#, fuzzy
msgid "View your search history"
msgstr "검색중"

#: opac-tmpl/ccsr/en/includes/top-bar.inc:60
#, c-format
msgid "Welcome, "
msgstr ""

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
#, fuzzy
msgid "You must be logged in to add tags."
msgstr "귀하는 지금처럼 로그인 되어 있습니다."

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
#, fuzzy
msgid "Your cart is currently empty"
msgstr "배달된 책 바구니"

#: opac-tmpl/ccsr/en/includes/top-bar.inc:6
#, c-format
msgid "Your cart is empty."
msgstr ""

#: opac-tmpl/ccsr/en/includes/top-bar.inc:29
#, fuzzy, c-format
msgid "Your lists"
msgstr "출판되지 않았습니다"

#. %1$s:  total |html 
#: opac-tmpl/ccsr/en/includes/masthead.inc:228
#, fuzzy, c-format
msgid "Your search returned %s results."
msgstr "배달된 책 바구니"

#. IMG
#: opac-tmpl/ccsr/en/includes/masthead.inc:16
#: opac-tmpl/ccsr/en/includes/masthead.inc:22
#, fuzzy
msgid "[% LibraryName %] Online Catalog"
msgstr "%1$s 장서목록"

#. INPUT type=text name=q
#: opac-tmpl/ccsr/en/includes/masthead.inc:91
msgid "[% ms_value |html %]"
msgstr ""

#. %1$s:  INCLUDE 'top-bar.inc' 
#: opac-tmpl/ccsr/en/includes/opac-bottom.inc:14
#, c-format
msgid ""
"[%%# Sticking the div for the top bar here; since the top bar is positioned "
"absolutely in this theme, it it makes the positioning of the rest of the "
"elements easier to keep it out of the doc3 div. %%] %s "
msgstr ""

#: opac-tmpl/ccsr/en/includes/top-bar.inc:41
#, fuzzy, c-format
msgid "[New list]"
msgstr "새 가상서가에 추가하세요:"

#: opac-tmpl/ccsr/en/includes/top-bar.inc:23
#, fuzzy, c-format
msgid "[View All]"
msgstr "대출 혹은 구독예약기간연장"

#: opac-tmpl/ccsr/en/includes/top-bar.inc:36
#, c-format
msgid "[View all]"
msgstr ""

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
#, fuzzy
msgid "already in your cart"
msgstr "도서관 안에서 사용가능함"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:13
#, c-format
msgid "change my password"
msgstr "내 패스워드를 변경하세요"

#. SCRIPT
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:63
#, fuzzy
msgid "item(s) added to your cart"
msgstr "책바구니로 보내기"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:6
#, c-format
msgid "my fines"
msgstr "나의 과료"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:33
#, fuzzy, c-format
msgid "my lists"
msgstr "서가에 추가하세요"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:30
#, fuzzy, c-format
msgid "my messaging"
msgstr "텍스트 메세지"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:8
#, c-format
msgid "my personal details"
msgstr "개인정보 세부사항"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:21
#, c-format
msgid "my privacy"
msgstr ""

#: opac-tmpl/ccsr/en/includes/usermenu.inc:26
#, fuzzy, c-format
msgid "my purchase suggestions"
msgstr "구입 제안"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:19
#, c-format
msgid "my reading history"
msgstr "내 독서기록"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:16
#, fuzzy, c-format
msgid "my search history"
msgstr "검색중"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:4
#, fuzzy, c-format
msgid "my summary"
msgstr "요약"

#: opac-tmpl/ccsr/en/includes/usermenu.inc:10
#, c-format
msgid "my tags"
msgstr ""

#. META http-equiv=Content-Type
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:2
msgid "text/html; charset=utf-8"
msgstr ""

#. LINK
#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:141
msgid "unAPI"
msgstr ""

#: opac-tmpl/ccsr/en/includes/doc-head-close.inc:25
msgid ""
"width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,"
"user-scalable=no"
msgstr ""

#: opac-tmpl/ccsr/en/includes/masthead.inc:195
#: opac-tmpl/ccsr/en/includes/masthead.inc:196
#: opac-tmpl/ccsr/en/includes/masthead.inc:197
#: opac-tmpl/ccsr/en/includes/masthead.inc:198
#: opac-tmpl/ccsr/en/includes/masthead.inc:199
#: opac-tmpl/ccsr/en/includes/masthead.inc:200
#: opac-tmpl/ccsr/en/includes/masthead.inc:202
#: opac-tmpl/ccsr/en/includes/masthead.inc:203
#, c-format
msgid "| "
msgstr ""
